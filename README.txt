This module provides a newsletter subscription block for Bring [http://www.bring.no/bring-dialog].

You need Bring API access to use this module.

You should this account data:
* Customer
* Api Address
* Authentication Key

To configure the block, go to:
  admin/structure/block/manage/bring_newsletter/bring_newsletter/configure
